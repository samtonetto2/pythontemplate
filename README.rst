Sample Module Repository
========================

This simple project is an example repo for Python projects.

`Learn more <http://www.kennethreitz.org/essays/repository-structure-and-python>`_.

Based on good practices outline at
https://docs.python-guide.org/writing/structure/

Taken from 
https://github.com/navdeep-G/samplemod

---------------

If you want to learn more about ``setup.py`` files, check out `this repository <https://github.com/kennethreitz/setup.py>`_.
